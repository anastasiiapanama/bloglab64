import React from 'react';
import './Navigation.css';

const Navigation = props => {
    return (
        <div className="Navigation">
            <ul className="Nav-list">
                <li className="Nav-item Border-link">
                    <a onClick={props.home} className="Nav-link">Home</a>
                </li>
                <li className="Nav-item Border-link">
                    <a onClick={props.add} className="Nav-link">Add</a>
                </li>
                <li className="Nav-item Border-link">
                    <a onClick={props.about} className="Nav-link">About</a>
                </li>
                <li className="Nav-item">
                    <a onClick={props.contacts} className="Nav-link">Contacts</a>
                </li>
            </ul>
        </div>
    );
};

export default Navigation;