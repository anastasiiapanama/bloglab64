import React, {useState, useEffect} from 'react';
import axiosPosts from "../../axios-posts";

import './EditPost.css';

const EditPost = props => {
    const id = props.match.params.id;

    const [editPost, setEditPost] = useState({
       title: '',
       text: ''
    });

    useEffect(() => {
       const fetchData = async () => {
           try {
               const response = await axiosPosts.get('/posts/' + id + '.json');

               setEditPost(response.data);
           } catch (e) {
               console.log('Network error');
           }
       };

       fetchData().catch(console.error);
    }, [id]);

    const changeHandler = event => {
        const {name, value} = event.target;

        setEditPost(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const editHandler = async event => {
      event.preventDefault();

      const editedPost = {
          title: editPost.title,
          text: editPost.text,
          date: editPost.date
      }

      try {
          await axiosPosts.put('/posts/' + id + '.json', editedPost);
      } finally {
          props.history.push('/');
      }
    };

    return (
        <div className="Container">
            <form className="Edit-form" onSubmit={editHandler}>
                <input
                    type="text" name="title"
                    value={editPost.title}
                    onChange={changeHandler}
                    className="Input-title"
                />
                <input
                    type="text" name="text"
                    value={editPost.text}
                    onChange={changeHandler}
                    className="Input-text"
                />

                <button className="Input-button">Edit</button>
            </form>
        </div>
    );
};

export default EditPost;