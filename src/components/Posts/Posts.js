import React from 'react';
import {NavLink} from "react-router-dom";
import moment from 'moment';

import './Posts.css';

const Posts = props => {
    return Object.keys(props.posts).map(post => {
        return (
            <div
                key={post}
                className="Post-item"
            >
                <p className="Post-date">Created on: {moment(props.posts[post].date).format('MMMM Do YYYY, h:mm:ss a')}</p>
                <h3 className="Post-title">{props.posts[post].title}</h3>
                <button className="Post-button" onClick={props.click}><NavLink to={'/posts/' + post}>Read more</NavLink></button>
            </div>
        )
    })
};

export default Posts;