import React, {useEffect, useState} from 'react';
import axiosPosts from "../../axios-posts";
import {NavLink} from "react-router-dom";

import './Post.css';

const Post = (props) => {
    const [postContent, setPostContent] = useState({});
    const id = props.match.params.id;

    useEffect(() => {
        const fetchData = async () => {
            try{
                const response = await axiosPosts.get('/posts/' + id + '.json');

                setPostContent(response.data);
            } catch (e) {
                console.error('Network error')
            };
        };

        fetchData().catch(console.error);
    }, [id]);

    const deletePost = async () => {
           try {
               await axiosPosts.delete('/posts/' + id + '.json');

               props.history.push('/');
           } catch (e) {
               console.error('Network error');
           }
    };

    return (
        <div className="Container">
            <div className="Post-block">
                <h3 className="Post-title">{postContent.title}</h3>
                <p className="Post-text">{postContent.text}</p>
                <button className="Edit-button"><NavLink to={'/posts/' + id + '/edit'}>Edit</NavLink></button>
                <button className="Delete-button" onClick={deletePost}>Delete</button>
            </div>
        </div>
    );
};

export default Post;