import React, {useEffect, useState} from 'react';
import Navigation from "../../components/Navigation/Navigation";

import './Home.css';
import axiosPosts from "../../axios-posts";
import Posts from "../../components/Posts/Posts";

const Home = props => {
    const [posts, setPosts] = useState({});

    const homeHandler = () => {
        props.history.push({
            pathname: '/'
        });
    };

    const addHandler = () => {
        props.history.push({
            pathname: '/posts/add'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axiosPosts.get('/posts.json');

                setPosts(response.data);

            } catch (e) {
                console.error('Network error');
            }
        };

        fetchData().catch(console.error);
    }, []);

    return (
        <div className="Container">
            <div className="Header-content">
                <h1 className="Title">My Blog</h1>

                <Navigation
                    home={homeHandler}
                    add={addHandler}
                    about={aboutHandler}
                    contacts={contactsHandler}
                />
            </div>

            {posts && <div className="Posts-block">
                <Posts
                    posts={posts}
                />
            </div>}
        </div>
    );
};

export default Home;