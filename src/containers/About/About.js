import React from 'react';
import Navigation from "../../components/Navigation/Navigation";

import './About.css';

const About = props => {

    const homeHandler = () => {
        props.history.push({
            pathname: '/'
        });
    };

    const addHandler = () => {
        props.history.push({
            pathname: '/posts/add'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="Container">
            <div className="Header-content">
                <h1 className="Title">My Blog</h1>

                <Navigation
                    home={homeHandler}
                    add={addHandler}
                    about={aboutHandler}
                    contacts={contactsHandler}
                />
            </div>

            <div className="About-block">
                <h2>About</h2>
                <p>What should an about us page say when everyone already knows who you are?
                    Well, National Geographic shows you everything you probably didn't know about it.
                    The first thing you see is a video with stunning imagery (as its known for) that
                    focuses on its conservation efforts and the global impact the company has had.
                    It's not until you've scrolled halfway down the page that it even mentions its
                    media, such as their iconic magazine and television network. This about us page
                    makes you quickly aware that the brand is more than just a nature magazine, it is
                    a powerful force in the global conservation effort.
                </p>
            </div>

        </div>
    );
};

export default About;