import React, {useState} from 'react';
import axiosPosts from "../../axios-posts";

import Navigation from "../../components/Navigation/Navigation";

import './AddPost.css';

const AddPost = props => {

    const [post, setPost] = useState({
        title: '',
        text: '',
    });

    const postDataChanged = event => {
        const {name, value} = event.target;

        setPost(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const postsHandler = async event => {
        event.preventDefault();

        const postOne = {
            title: post.title,
            text: post.text,
            date: new Date()
        };

        try {
            await axiosPosts.post('/posts.json', postOne);
        } finally {
            props.history.push('/');
        }
    };

    const homeHandler = () => {
        props.history.push({
            pathname: '/'
        });
    };

    const addHandler = () => {
        props.history.push({
            pathname: '/posts/add'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="Container">
            <div className="Header-content">
                <h1 className="Title">My Blog</h1>

                <Navigation
                    home={homeHandler}
                    add={addHandler}
                    about={aboutHandler}
                    contacts={contactsHandler}
                />
            </div>

            <div className="Add-block">
                <h2>Add new post</h2>

                <form className="Form-block" onSubmit={postsHandler}>
                    <h3>Title</h3>
                    <input
                        className="Input-title-post"
                        type="text" name="title"
                        value={post.title}
                        onChange={postDataChanged}
                    />

                    <h3>Description</h3>

                    <input
                        className="Input-text-post"
                        type="text" name="text"
                        value={post.text}
                        onChange={postDataChanged}
                    />

                    <button className="Add-button">Save</button>
                </form>
            </div>
        </div>
    );
};

export default AddPost;