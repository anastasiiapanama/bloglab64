import React from 'react';
import Navigation from "../../components/Navigation/Navigation";

import './Contacts.css';

const Contacts = props => {

    const homeHandler = () => {
        props.history.push({
            pathname: '/'
        });
    };

    const addHandler = () => {
        props.history.push({
            pathname: '/posts/add'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="Container">
            <div className="Header-content">
                <h1 className="Title">My Blog</h1>

                <Navigation
                    home={homeHandler}
                    add={addHandler}
                    about={aboutHandler}
                    contacts={contactsHandler}
                />
            </div>

            <div className="Contacts-block">
                <h2>Contacts</h2>
                <p>Accumsan ullamcorper turpis malesuada erat massa maecenas aliquam quam netus dui lacus at, suspendisse nullam fusce rutrum sed rhoncus conubia dictumst class laoreet. Risus facilisis litora bibendum imperdiet eros sit felis, tincidunt convallis maecenas porttitor proin urna, varius semper dui conubia dis mollis. Ex lobortis dolor ac est viverra massa fames tristique, pretium suspendisse dictum placerat consectetur nisl fusce elit luctus, dapibus bibendum malesuada fringilla a porttitor dictumst. </p>
            </div>

        </div>
    );
};

export default Contacts;