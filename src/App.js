import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Home from "./containers/Home/Home";
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";
import AddPost from "./containers/AddPost/AddPost";
import Post from "./components/Post/Post";
import EditPost from "./components/EditPost/EditPost";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/posts/add" component={AddPost} />
        <Route path="/posts/:id" exact component={Post} />
        <Route path="/posts/:id/edit" exact component={EditPost} />
        <Route path="/about" component={About} />
        <Route path="/contacts" component={Contacts} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
