import axios from "axios";

const axiosPosts = axios.create({
   baseURL: 'https://blog-lab64-ponamareva-default-rtdb.firebaseio.com/'
});

export default axiosPosts;